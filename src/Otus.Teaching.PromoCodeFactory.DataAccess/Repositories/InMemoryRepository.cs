﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>, IDisposable
        where T : BaseEntity
    {
        protected IList<T> Data { get; set; }
        private readonly Mutex mutex = new Mutex();
        private bool disposedValue;

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new List<T>(data);
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            mutex.WaitOne();
            try
            {
                return Task.FromResult(Data.AsEnumerable());
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            mutex.WaitOne();
            try
            {
                return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
            }
            finally
            {
                mutex.ReleaseMutex();
            }
        }

        public Task<T> AddAsync(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            return Task.Run(() =>
            {
                mutex.WaitOne();
                try
                {
                    entity.Id = Guid.NewGuid();
                    Data.Add(entity);
                    return entity;
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            });
        }

        public Task UpdateAsync(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            return Task.Run(() =>
            {
                mutex.WaitOne();
                try
                {
                    var result = Data.FirstOrDefault(e => e.Id == entity.Id);
                    if (result == null)
                        return;
                    Data[Data.IndexOf(result)] = entity;
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            });
        }

        public Task DeleteAsync(T entity)
        {
            if (entity is null)
                throw new ArgumentNullException(nameof(entity));

            return Task.Run(() =>
            {
                mutex.WaitOne();
                try
                {
                    var result = Data.FirstOrDefault(e => e.Id == entity.Id);
                    if (result == null)
                        return;
                    Data.Remove(result);
                }
                finally
                {
                    mutex.ReleaseMutex();
                }
            });
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    mutex.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}